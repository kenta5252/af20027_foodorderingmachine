import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton karaageButton;
    private JButton ramenButton;
    private JButton chahanButton;
    private JButton gyouzaButton;
    private JButton harumakiButton;
    private JButton syuumaiButton;
    private JTextPane orderedItemsList;
    private JButton checkoutButton;
    private JButton oomoriButton;
    private JButton komoriButton;
    private JLabel totalbox;

    public FoodOrderingMachine() {

        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.jpg")));
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.jpg")));
        chahanButton.setIcon(new ImageIcon(
                this.getClass().getResource("chahan.jpg")));
        gyouzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("gyouza.jpg")));
        harumakiButton.setIcon(new ImageIcon(
                this.getClass().getResource("harumaki.jpg")));
        syuumaiButton.setIcon(new ImageIcon(
                this.getClass().getResource("syuumai.jpg")));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 500);
            }
        });

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 700);
            }
        });

        chahanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chahan", 600);
            }
        });

        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyouza", 450);
            }
        });

        harumakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Harumaki", 350);
            }
        });

        syuumaiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Syuumai", 300);
            }
        });

        komoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Komori();
            }
        });

        oomoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Oomori();
            }
        });

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
    }
    int sum = 0;
    int komori, oomori;
    void order(String food, int price){

        totalbox.setText("Total   " + sum + "   Yen");

        if(komori == -40){
            price -= 40;
            komori = 0;
        }
        if(oomori == +40){
            price += 40;
            oomori = 0;
        }

        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + ". (" + price + " Yen)",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + "  " + price + " Yen\n");
            sum += price;
            totalbox.setText("Total " + sum + " Yen");
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for your order !!\n" +
                            "Order for " + food + " received.");
        }
    }

    void Komori(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like 'Komori' of your next order ? ",
                "'Komori' Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + "(next menu is Komori)\n");
            JOptionPane.showMessageDialog(
                    null,
                    "The next menu will be Komori !!!");
            komori = -40;
        }
    }

    void Oomori(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like 'Oomori' of your next order ? ",
                "'Oomori' Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + "(next menu is Oomori)\n");
            JOptionPane.showMessageDialog(
                    null,
                    "The next menu will be Oomori !!!");
            oomori = +40;
        }
    }

    void checkout(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout ? ",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you. The total price is " + sum + " yen.");
            orderedItemsList.setText("");
            sum = 0;
            totalbox.setText("Total " + sum + " Yen");
        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
